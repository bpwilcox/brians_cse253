%% Generates test cases and plots for Programming Assignment 1
clear all

%% This folder has the loading code used to access the MNIST data
addpath('mnistHelper');

%% Load the data, take only the first 20000 training points and 2000 test points
images = loadMNISTImages('mnistdata/train-images.idx3-ubyte');
labels = loadMNISTLabels('mnistdata/train-labels.idx1-ubyte');
sim = loadMNISTImages('mnistdata/t10k-images.idx3-ubyte');
slb = loadMNISTLabels('mnistdata/t10k-labels.idx1-ubyte');

% Cut down to 20000 train, 2000 test
images = images(:, 1:20000);
labels = labels(1:20000);
sim = sim(:, 1:2000);
slb = slb(1:2000);

% Throw out all but two digits
a = 2;
b = 8;
tvt = logical((labels == a) + (labels == b));
labs = labels(tvt);
dats = images(:, tvt);
labs(labs == a) = 1;
labs(labs == b) = 2;
N = numel(labs);

% Same for test data
tst = logical((slb == a) + (slb == b));
sim = sim(:, tst);
slb = slb(tst);
slb(slb == a) = 1;
slb(slb == b) = 2;
Nt = numel(slb);

% Develop a hold out set
hopct = 0.1;
hoind = round((1 - hopct) * N);
hoi = dats(:, hoind : end);
hol = labs(hoind : end);
tri = dats(:, 1 : (hoind - 1));
trl = labs(1 : (hoind - 1));

% Set the data and parameters for the experiment
p.tri = tri;
p.trl = trl;
p.hoi = hoi;
p.hol = hol;
p.tsi = sim;
p.tsl = slb;
p.eta = 0.01;
p.trmin = 0.001;
p.maxit = 64;
p.mom = 0.8;
p.l1 = 0;
p.l2 = 0;

% Run the experiment and gather the percent error on the test data!
[w, out] = logreg(p);
pc = classifier(w, sim, slb);
