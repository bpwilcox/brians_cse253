%% Test cases and plots for PA2

%% Problem 3) Base case - 1 hidden layer, include momentum, regularization,
%annealing

testcomp = struct;
k=0;

%Import data

%% Generates test cases and plots for Programming Assignment 1
clear all

%% This folder has the loading code used to access the MNIST data
addpath('mnistHelper');

%% Load the data, take only the first 20000 training points and 2000 test points
images = loadMNISTImages('mnistdata/train-images.idx3-ubyte');
labels = loadMNISTLabels('mnistdata/train-labels.idx1-ubyte');
sim = loadMNISTImages('mnistdata/t10k-images.idx3-ubyte');
slb = loadMNISTLabels('mnistdata/t10k-labels.idx1-ubyte');

%% Pre-process
% images = images/255;
images = images -mean(images);
% sim = sim/255;
sim = sim-mean(sim);


%% Cut down to 20000 train, 2000 test
trkeep = length(labels);
tskeep = length(slb);
images = images(:, 1:trkeep);
labels = labels(1:trkeep);
sim = sim(:, 1:tskeep);
slb = slb(1:tskeep);

%% Throw out all but some digits!
% digs = [2, 3];
digs = 0:9;
inds = logical(any(labels == digs, 2));
labs = labels(inds);
dats = images(:, inds);
[~, labs] = max((labs == digs), [], 2);
N = numel(labs);
dats = [ones(1, N); dats];
tri = dats;
trl = labs;

% Same for test data
tst = logical(any(slb == digs, 2));
sim = sim(:, tst);
slb = slb(tst);
[~, slb] = max(slb == digs, [], 2);
Nt = numel(slb);
sim = [ones(1, Nt); sim];

%% Develop a hold out set
% hopct = 0.0;
% hoind = round((1 - hopct) * N);
% hoi = dats(:, hoind : end);
% hol = labs(hoind : end);
% tri = dats(:, 1 : (hoind - 1));
% trl = labs(1 : (hoind - 1));

%%Weight initialization (zeros for part Problem 3)
d = size(tri, 1);
ncl = max(trl);

weight_opts.layers = [300];
weight_opts.funs = {'sigmoid','softmax'};
weight_opts.distr = 'zeros';
W = initw(weight_opts,d,ncl);

%% Set the data and parameters for the experiment
batchsize = N;

p.tri = tri;
p.trl = trl;
% p.hoi = hoi;
% p.hol = hol;
% p.tsi = sim;
% p.tsl = slb;
p.eta = 0.05;
p.shuffle = 0;
p.ann = 'hyp';
T = 200;
p.annpar = 1 / T;
p.trmin = 0.01;
p.maxit = 512;
p.esi = 3;
p.mom = 0;
p.l1 = 0;
p.l2 = 0.0005;
p.reprate = round(N / batchsize);
p.trdelmin = 0.0001;
% p.netpars.layers = [128];
% p.netpars.funs = {'sigmoid', 'softmax'};
% p.netpars.distr = 'randnorm';
p.winit = W;

p = netregdefs(p);


%% Run Algorithm and Generate Plots

x = p.tri;
labs = p.trl;
[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);

epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Base Case';
filename = 'base';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);


%% Check gradient comparison
gradestprep;
gradest;

%% Problem 4) Tricks of trade

%% a) Shuffle

batchsize = N;
p.shuffle = 1;
p.reprate = round(N / batchsize);

%% Run Algorithm and Classify

[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);

epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Shuffle';
filename = 'shuffle';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);

%% c) Use tanh sigmoid

weight_opts.layers = [300];
weight_opts.funs = {'tanh','softmax'};
weight_opts.distr = 'zeros';
W = initw(weight_opts,d,ncl);
p.winit = W;

%% Run Algorithm and Classify

[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);


epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Using tanh as sigmoid';
filename = 'sig_tanh';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);

%% d) Random normal weight distrubiton with mean and std 1/sqrt(fan-in)

weight_opts.layers = [300];
weight_opts.funs = {'tanh','softmax'};
weight_opts.distr = 'randnorm';
W = initw(weight_opts,d,ncl);
p.winit = W;

%% Run Algorithm and Classify

[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);

epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Random normal weight initialization';
filename = 'rnweights';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);
%% e) use momentum

p.mom = 0.9;

%% Run Algorithm and Classify

[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);

epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Including momentum';
filename = 'momentum';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);
%% 5) Network Topology


%% a) Double number of hidden units

weight_opts.layers = [600];
weight_opts.funs = {'tanh','softmax'};
weight_opts.distr = 'randnorm';
W = initw(weight_opts,d,ncl);
p.winit = W;

%% Run Algorithm and Classify

[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);


epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Double hidden units';
filename = 'double';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);
%% a.2) Half number of hidden units

weight_opts.layers = [150];
weight_opts.funs = {'tanh','softmax'};
weight_opts.distr = 'randnorm';
W = initw(weight_opts,d,ncl);
p.winit = W;

%% Run Algorithm and Classify

[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);

epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Half Hidden Units';
filename = 'half';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);
%% b) Add another hidden layer

weight_opts.layers = [300,300];
weight_opts.funs = {'tanh','tanh','softmax'};
weight_opts.distr = 'randnorm';
W = initw(weight_opts,d,ncl);
p.winit = W;

%% Run Algorithm and Classify

[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);

epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Add second hidden layer';
filename = 'twohidlayers';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);
%% b.2 Change hidden layers size

weight_opts.layers = [300,100];
weight_opts.funs = {'tanh','tanh','softmax'};
weight_opts.distr = 'randnorm';
W = initw(weight_opts,d,ncl);
p.winit = W;

%% Run Algorithm and Classify

[w, out] = netreg(p);
[classtrain, tpe] = classifier(w, x, labs);
[classtest, spe] = classifier(w, sim, slb);


epochs = 1/p.reprate:1/p.reprate:length(out1.trc)/p.reprate;
titlep = 'Change units of both hidden layer';
filename = 'twohiddif';
makeplots(epochs,out.tpe,out.spe,titlep,filename)

k=k+1;
testcomp(k).test = filename;
testcomp(k).numits = length(epochs);
testcomp(k).accuracy_train = 1-min(tpe);
testcomp(k).accuracy_test = 1-min(spe);