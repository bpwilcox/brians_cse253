%% Generates test cases and plots for Programming Assignment 1
clear all

%% This folder has the loading code used to access the MNIST data
addpath('mnistHelper');

%% Load the data, take only the first 20000 training points and 2000 test points
images = loadMNISTImages('mnistdata/train-images.idx3-ubyte');
labels = loadMNISTLabels('mnistdata/train-labels.idx1-ubyte');
sim = loadMNISTImages('mnistdata/t10k-images.idx3-ubyte');
slb = loadMNISTLabels('mnistdata/t10k-labels.idx1-ubyte');

% Cut down to 20000 train, 2000 test
images = images(:, 1:20000);
labels = labels(1:20000);
sim = sim(:, 1:2000);
slb = slb(1:2000);

% Throw out all but some digits!
% digs = [2, 3];
digs = 0:9;
inds = logical(any(labels == digs, 2));
labs = labels(inds);
dats = images(:, inds);
[~, labs] = max((labs == digs), [], 2);
N = numel(labs);
dats = [ones(1, N); dats];

% Same for test data
tst = logical(any(slb == digs, 2));
sim = sim(:, tst);
slb = slb(tst);
[~, slb] = max(slb == digs, [], 2);
Nt = numel(slb);
sim = [ones(1, Nt); sim];

% Develop a hold out set
hopct = 0.1;
hoind = round((1 - hopct) * N);
hoi = dats(:, hoind : end);
hol = labs(hoind : end);
tri = dats(:, 1 : (hoind - 1));
trl = labs(1 : (hoind - 1));

% Set the data and parameters for the experiment
p.tri = tri;
p.trl = trl;
p.hoi = hoi;
p.hol = hol;
p.tsi = sim;
p.tsl = slb;
p.eta = 0.05;
p.ann = 'hyp';
p.annpar = 1 / 200;
p.trmin = 0.01;
p.maxit = 512;
p.esi = 3;
p.mom = 0.9;
p.l1 = 0;
p.l2 = 0.001;
p.trdelmin = 0.0001;

% Run the experiment and gather the percent error on the test data!
% tic
% [w, out] = logreged(p);
% toc
% disp('Time after logreged');
% p.l1, p.l2, out.tse(end)
tic
[w, out] = logreg(p);
toc
disp('Time after logreg');
p.l1, p.l2, out.tse(end)
